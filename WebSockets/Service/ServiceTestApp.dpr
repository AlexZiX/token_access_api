program ServiceTestApp;

uses
  Vcl.Forms,
  uTest in 'uTest.pas' {Form1},
  wsService in 'src\wsService.pas',
  pkcs11t in 'src\pkcs11t.pas',
  TokenAccess in 'src\TokenAccess.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
