unit TokenAccess;

interface

uses pkcs11t, System.Generics.Collections, WinApi.Windows, System.SysUtils,
     System.JSON, System.IOUtils, System.StrUtils;

type
  TTokenType = (ruToken = 0, iKey = 1, eToken = 2, ttNone = 3);

  RToken = record
    SmartCardID: String[16];
    slotID: CK_SLOT_ID;
    TokenType: TTokenType; // 1 - RuToken, 2 - iKey, 3 - eToken
    TokenModel: String[16];
    TokenName: String[32];
  end;

  CK_RUTOKEN_INIT_PARAM = packed record
    ulSizeofThisStructure: CK_ULONG;
    UseRepairMode: CK_ULONG;
    pNewAdminPin: CK_BYTE_PTR;
    ulNewAdminPinLen: CK_ULONG;
    pNewUserPin: CK_BYTE_PTR;
    ulNewUserPinLen: CK_ULONG;
    ChangeUserPINPolicy: CK_FLAGS;
    ulMinAdminPinLen: CK_ULONG;
    ulMinUserPinLen: CK_ULONG;
    ulMaxAdminRetryCount: CK_ULONG;
    ulMaxUserRetryCount: CK_ULONG;
    pTokenLabel: CK_BYTE_PTR;
    ulLabelLen: CK_ULONG;
    ulSmMode: CK_ULONG;
  end;

  CK_RUTOKEN_INIT_PARAM_PTR = ^CK_RUTOKEN_INIT_PARAM;

  TLogger = class
  public
    class procedure WriteLog(LogMsg: String);
  end;

  TToken = class
  private
    TokenType: TTokenType;
    PKCS11LibName: String;
    TokensList: TList<RToken>;
    PKCS11LibHandle: THandle;

    // PKCS#11 functions
    C_Initialize: function(pInitArgs: CK_VOID_PTR): CK_RV; cdecl;
    C_GetSlotList: function(tokenPresent: CK_BBOOL; pSlotList: CK_SLOT_ID_PTR; pulCount: CK_ULONG_PTR): CK_RV; cdecl;
    C_GetSlotInfo: function(slotID: CK_SLOT_ID; pInfo: CK_SLOT_INFO_PTR): CK_RV; cdecl;
    C_GetTokenInfo: function(slotID: CK_SLOT_ID; pInfo: CK_TOKEN_INFO_PTR): CK_RV; cdecl;
    C_Finalize: function(pReserved: CK_VOID_PTR): CK_RV; cdecl;
    C_OpenSession: function(slotID: CK_SLOT_ID; flags: CK_FLAGS; pApplication: CK_VOID_PTR; Notify: CK_NOTIFY; phSession: CK_SESSION_HANDLE_PTR): CK_RV; cdecl;
    C_CloseSession: function(hSession: CK_SESSION_HANDLE): CK_RV; cdecl;
    C_Login: function(hSession: CK_SESSION_HANDLE; userType: CK_USER_TYPE; pPin: CK_UTF8CHAR_PTR; ulPinLen: CK_ULONG): CK_RV; cdecl;
    C_Logout: function(hSession: CK_SESSION_HANDLE): CK_RV; cdecl;
    C_InitPIN: function(hSession: CK_SESSION_HANDLE; pPin: CK_UTF8CHAR_PTR; ulPinLen: CK_ULONG): CK_RV; cdecl;
    C_SetPIN: function(hSession: CK_SESSION_HANDLE; pOldPin: CK_UTF8CHAR_PTR; ulOldPinLen: CK_ULONG; pNewPin: CK_UTF8CHAR_PTR; ulNewPinLen: CK_ULONG): CK_RV; cdecl;
    C_InitToken: function(slotID: CK_SLOT_ID; pPin: CK_UTF8CHAR_PTR; ulPinLen: CK_ULONG; pLabel: CK_UTF8CHAR_PTR): CK_RV; cdecl;
    C_FindObjectsInit: function(hSession: CK_SESSION_HANDLE; pTemplate: CK_ATTRIBUTE_PTR; ulCount: CK_ULONG): CK_RV; cdecl;
    C_FindObjects: function(hSession: CK_SESSION_HANDLE; phObject: CK_OBJECT_HANDLE_PTR; ulMaxObjectCount: CK_ULONG; pulObjectCount: CK_ULONG_PTR): CK_RV; cdecl;
    C_FindObjectsFinal: function(hSession: CK_SESSION_HANDLE): CK_RV; cdecl;
    C_CreateObject: function(hSession: CK_SESSION_HANDLE; pTemplate: CK_ATTRIBUTE_PTR; ulCount: CK_ULONG; phObject: CK_OBJECT_HANDLE_PTR): CK_RV; cdecl;
    C_DestroyObject: function(hSession: CK_SESSION_HANDLE; hObject: CK_OBJECT_HANDLE): CK_RV; cdecl;
    C_DigestInit: function(hSession: CK_SESSION_HANDLE; pMechanism: CK_MECHANISM_PTR): CK_RV; cdecl;
    C_Digest: function(hSession: CK_SESSION_HANDLE; pData: CK_BYTE_PTR; ulDataLen: CK_ULONG; pDigest: CK_BYTE_PTR; pulDigestLen: CK_ULONG_PTR): CK_RV; cdecl;

    procedure LoadPKCS11Dll;
    procedure UnloadPKCS11Dll;

    function GetStringFromByteArray(ByteArray: array of byte): String;
    function GetStringFromUTF8ByteArray(ByteArray: array of Byte): String;
  public
    procedure ListTokensToJSON(jsonResult: TJSONObject);
    procedure ListTokens;
    function GetTokenBySmartCardID(SmartCardID: String): RToken;
    procedure SetTokenName(SmartCardID, TokenName, UserPIN: String); virtual; abstract;
    procedure SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN, NewAdministratorPIN: String); virtual;
    procedure SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN: String); virtual;
    procedure FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
      AdministratorPINTriesCount, UserPINTriesCount: Integer); virtual; abstract;

    constructor Create;
    destructor Destroy; override;
  end;

  TRuToken = class(TToken)
    // PKCS #11 library for ruToken
    const ruTokenLib = 'rtpkcs11.dll';
  private
    // ruToken specific PKCS#11 functions
    RuToken_C_EX_SetTokenName: function(hSession: CK_SESSION_HANDLE; pLabel: CK_BYTE_PTR; ulLabelLen: CK_ULONG): CK_RV; cdecl;
    RuToken_C_EX_InitToken: function(slotID: CK_SLOT_ID; pPin: CK_UTF8CHAR_PTR; ulPinLen: CK_ULONG; pInitInfo: CK_RUTOKEN_INIT_PARAM_PTR): CK_RV; cdecl;

    procedure LoadPKCS11Dll;
  public
    procedure SetTokenName(SmartCardID, TokenName, UserPIN: String); override;
    procedure FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
      AdministratorPINTriesCount, UserPINTriesCount: Integer); override;

    constructor Create;
  end;

  TIKey = class(TToken)
    // PKCS #11 library for iKey 1000
    const iKeyTokenLib = 'K1PK112.dll';
    // iKey API library
    const iKeyAPILib = 'iKeyAPI.dll';
    const iKeyAPIVersion = $200;

    // iKey constants
    const IKEY_ROOT_DIR           = $00000000;
    const IKEY_CHANGE_USER_PIN    = $00000000;
    const IKEY_OPEN_FIRST         = $00000001;
    const IKEY_VERIFY_SO_PIN      = $00000001;
    const IKEY_UNBLOCK_USER_PIN   = $00000001;
    const IKEY_CHANGE_SO_PIN      = $00000002;
    const IKEY_OPEN_SPECIFIC      = $00000003;
    const IKEY_PROP_SERNUM        = $00000007;
    const IKEY_PROP_FRIENDLY_NAME = $0000000B;
    const IKEY_OPEN_BY_NAME       = $00000100;
    const IKEY_DIR_BY_LONG_ID     = $00000200;
    const IKEY_DIR_BY_GUID_STR    = $00000500;
    const IKEY_DELETE_RECURSIVE   = $00010000;
  private
    iKeyAPILibHandle: THandle;

    // iKey API functions
    iKey_CreateContext: function(iHandle: CK_VOID_PTR; Flags: CK_ULONG; ApiVersion: CK_ULONG): CK_RV; stdcall;
    iKey_OpenDevice: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; pApp: CK_VOID_PTR_PTR): CK_RV; stdcall;
    iKey_CloseDevice: function(iHandle: CK_SESSION_HANDLE): CK_RV; stdcall;
    iKey_SetProperty: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; RefData: CK_VOID_PTR; PropData: CK_VOID_PTR; PropSize: CK_ULONG): CK_RV; stdcall;
    iKey_GetProperty: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; RefData: CK_VOID_PTR; PropData: CK_VOID_PTR; PropSize: CK_ULONG): CK_RV; stdcall;
    iKey_Verify: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; Data: CK_VOID_PTR; DataSize: CK_ULONG): CK_RV; stdcall;
    iKey_DeleteDir: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; DirID: CK_ULONG; Name: CK_VOID_PTR): CK_RV; stdcall;
    iKey_ChangeCode: function(iHandle: CK_SESSION_HANDLE; Flags: CK_ULONG; OldCode: CK_UTF8CHAR_PTR; OldCodeLen: CK_ULONG; NewCode: CK_UTF8CHAR_PTR; NewCodeLen: CK_ULONG): CK_RV; stdcall;

    procedure LoadIKeyAPIDll;
    procedure UnloadIKeyAPIDll;
  public
    procedure SetTokenName(SmartCardID, TokenName, UserPIN: String); override;
    procedure SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN, NewAdministratorPIN: String); override;
    procedure SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN: String); override;
    procedure FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
      AdministratorPINTriesCount, UserPINTriesCount: Integer); override;

    constructor Create;
    destructor Destroy; override;
  end;

  TEToken = class(TToken)
    // PKCS #11 library for eToken
    const eTokenLib = 'eTPKCS11.dll';
    const SAPILib = 'eTSAPI.dll';

    // eToken specific constants
    const ETCKH_TOKEN_OBJECT = $80005002;
    const ETCKA_RSA_2048 = $80001121;
    const ETCKA_FIPS = $8000111e;

    // SAPI specific constants
    const CKA_SAPI_RETRY_USER_MAX = $80001112;
    const CKA_SAPI_RETRY_SO_MAX = $80001113;
    const CKA_SAPI_PIN_USER = $80001114;
    const CKA_SAPI_PIN_SO = $80001115;
    const CKA_SAPI_PIN_CURRENT = $80001116;
    const CKA_SAPI_OLD_KEY = $80001117;
    const CKA_SAPI_NEW_KEY = $80001118;
  private
    eTokenSAPILibHandle: THandle;

    // eToken PKCS#11 specific functions
    eTokenFunctionsList: ETCK_FUNCTION_LIST_EX_PTR;

    SAPI_SetTokenName: function(hSession: CK_SESSION_HANDLE; pLabel: CK_CHAR_PTR): CK_RV; cdecl;
    SAPI_InitToken: function(SlotID: CK_SLOT_ID; pTemplate: CK_ATTRIBUTE_PTR; ulCount: CK_ULONG; pContext: CK_VOID_PTR; Callback: Pointer): CK_RV; cdecl;

    // The function returns pointer to list of extension functions specific for eToken
    // This functions isn't exported and available only by pointers (don't ask me why)
    // Just read SAC Developers Guide p.50
    ETC_GetFunctionListEx: function(ppFunctionList: ETCK_FUNCTION_LIST_EX_PTR_PTR): CK_RV; cdecl;

    procedure LoadPKCS11Dll;

    procedure LoadSAPIDll;
    procedure UnloadSAPIDll;

    procedure HashStartKey(buffer: String; pDigest: CK_BYTE_PTR; ulKeyLen: CK_ULONG);
    procedure CreateKeyObject(hSession: CK_SESSION_HANDLE; pLabel, pKey: String);
  public
    procedure SetTokenName(SmartCardID, TokenName, UserPIN: String); override;
    procedure FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
      AdministratorPINTriesCount, UserPINTriesCount: Integer); override;

    constructor Create;
    destructor Destroy; override;
  end;

  TTokenAccess = class
  private
    procedure ListTokensToJSON(TokenType: TTokenType; jsonResult: TJSONObject);
    procedure GetTokenNameBySmartCardID(SmartCardID: String; TokenType: TTokenType; jsonResult: TJSONObject);
    procedure SetTokenName(SmartCardID, TokenName, UserPIN: String; TokenType: TTokenType);
    procedure SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN: String;
      TokenType: TTokenType);
    procedure SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN, NewAdministratorPIN: String;
      TokenType: TTokenType);
    procedure FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
      AdministratorPINTriesCount, UserPINTriesCount: Integer;
      TokenType: TTokenType);

    function GetJSONStringParameter(jsonInput: TJSONObject; ParName: String): String;
  public
    class function GetErrorMessageByCode(ErrorCode: Word): String;

    function ProcessJSONRequest(JSONStr: String): String;
  end;

  ETokenAccessException = class(Exception)
  private
    FErrorCode: Word;
    FNativeErrorCode: Word;
  public
    constructor Create(ErrorCode: Word; NativeErrorCode: Word = 0);
    property ErrorCode: Word read FErrorCode;
    property NativeErrorCode: Word read FNativeErrorCode;
  end;

implementation

{ TTokenAccess }

procedure TTokenAccess.FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
  NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
  AdministratorPINTriesCount, UserPINTriesCount: Integer;
  TokenType: TTokenType);
var Token: TToken;
begin
  if Length(TokenName) > 16 then
    raise ETokenAccessException.Create(23);

  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    Token.FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
      NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey,
      AdministratorPINTriesCount, UserPINTriesCount);
  finally
    Token.Free;
  end;
end;

class function TTokenAccess.GetErrorMessageByCode(ErrorCode: Word): String;
begin
  case ErrorCode of
     1: Result := 'Can''t initialize PKCS#11 library.';
     2: Result := 'Can''t get slot list.';
     3: Result := 'Can''t get slot info.';
     4: Result := 'Can''t get token info.';
     5: Result := 'Smart Card with the specified ID wasn''t found.';
     6: Result := 'Can''t open session with token.';
     7: Result := 'Can''t login user to token.';
     8: Result := 'Can''t set token name.';
     9: Result := 'Can''t logout user from token.';
    10: Result := 'Can''t close session with token.';
    11: Result := 'Not enough input parameters.';
    12: Result := 'Can''t load PKCS#11 library.';
    13: Result := 'Can''t load functions from PKCS#11 library.';
    14: Result := 'Can''t initialize token.';
    15: Result := 'Can''t initialize iKey API library.';
    16: Result := 'Can''t load functions from iKey API library.';
    17: Result := 'Can''t load iKey API library.';
    18: Result := 'Can''t create iKey context.';
    19: Result := 'Can''t open iKey device.';
    20: Result := 'Can''t set iKey property.';
    21: Result := 'Can''t close iKey device.';
    22: Result := 'Can''t get iKey property.';
    23: Result := 'Token name is too long. Maximum 16 characters allowed.';
    24: Result := 'Administrator or user PIN tries count is too big. Maximum 10 tries allowed.';
    25: Result := 'Administrator and user PINs shold be between 6 and 32 characters long.';
    26: Result := 'iKey administrator PIN verification failed.';
    27: Result := 'Can''t delete PKI Storage from iKey';
    28: Result := 'Unknown token type.';
    29: Result := 'Can''t change iKey PIN.';
    30: Result := 'Can''t init iKey device.';
    31: Result := 'Administrator PIN shold be between 6 and 25 characters long.';
    32: Result := 'User PIN shold be between 1 and 10 numeric characters long.';
    33: Result := 'Can''t init eToken device.';
    34: Result := 'Can''t init objects search.';
    35: Result := 'Can''t complete objects searching.';
    36: Result := 'Can''t finalize object searching.';
    37: Result := 'Can''t destroy object.';
    38: Result := 'Can''t load extension functions list from eToken PKCS#11 library.';
    39: Result := 'Can''t init token initialization process.';
    40: Result := 'Can''t set user PIN';
    41: Result := 'Can''t finalize token initialization process.';
    42: Result := 'Can''t create virtual session.';
    43: Result := 'Can''t init digest.';
    44: Result := 'Can''t create digest.';
    45: Result := 'Can''t create object.';
    46: Result := 'Can''t initialize eToken SAPI library.';
    47: Result := 'Can''t load functions from eToken SAPI library.';
    else Result := 'Unknown error.';
  end;
end;

function TTokenAccess.GetJSONStringParameter(jsonInput: TJSONObject;
  ParName: String): String;
var jsonValue: TJSONValue;
begin
  jsonValue := jsonInput.GetValue(ParName);
  if jsonValue <> nil then
    Result := StringReplace(jsonValue.ToString, '"', '', [rfReplaceAll])
  else
    raise ETokenAccessException.Create(11);
end;

procedure TTokenAccess.GetTokenNameBySmartCardID(SmartCardID: String;
  TokenType: TTokenType; jsonResult: TJSONObject);
var TokenRec: RToken;
    Token: TToken;
begin
  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    TokenRec := Token.GetTokenBySmartCardID(SmartCardID);
    jsonResult.AddPair(TJSONPair.Create('SmartCardName', TokenRec.TokenName));
  finally
    Token.Free;
  end;
end;

procedure TTokenAccess.ListTokensToJSON(TokenType: TTokenType; jsonResult: TJSONObject);
var Token: TToken;
begin
  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    Token.ListTokensToJSON(jsonResult);
  finally
    Token.Free;
  end;
end;

function TTokenAccess.ProcessJSONRequest(JSONStr: String): String;
var jsonInput: TJSONObject;
    jsonResult: TJSONObject;
    Command: String;
    SmartCardID: String;
    TokenName, TokenType, CurInitKey, NewInitKey: String;
    AdministratorPIN, UserPIN: String;
    CurrentAdministratorPIN, NewAdministratorPIN, NewUserPIN: String;
    AdministratorPINTriesCount, UserPINTriesCount: Integer;
    LogMsg: String;
begin
  // Result object
  jsonResult := TJSONObject.Create;

  try
    // Parse incoming json
    jsonInput := TJSONObject.ParseJSONValue(JSONStr) as TJSONObject;

    try
      // Find which command should be executed
      Command := GetJSONStringParameter(jsonInput, 'Command');

      // And execute it
      if Command = 'ListTokens' then
      begin
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        ListTokensToJSON(TTokenType(StrToInt(TokenType)), jsonResult);
      end;

      if Command = 'GetTokenNameBySmartCardID' then
      begin
        SmartCardID := GetJSONStringParameter(jsonInput, 'SmartCardID');
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        GetTokenNameBySmartCardID(SmartCardID, TTokenType(StrToInt(TokenType)), jsonResult);
      end;

      if Command = 'SetTokenName' then
      begin
        SmartCardID := GetJSONStringParameter(jsonInput, 'SmartCardID');
        TokenName := GetJSONStringParameter(jsonInput, 'TokenName');
        UserPIN := GetJSONStringParameter(jsonInput, 'UserPIN');
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        SetTokenName(SmartCardID, TokenName, UserPIN, TTokenType(StrToInt(TokenType)));
      end;

      if Command = 'SetAdministratorPIN' then
      begin
        SmartCardID := GetJSONStringParameter(jsonInput, 'SmartCardID');
        CurrentAdministratorPIN := GetJSONStringParameter(jsonInput, 'CurrentAdministratorPIN');
        NewAdministratorPIN := GetJSONStringParameter(jsonInput, 'NewAdministratorPIN');
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN, NewAdministratorPIN, TTokenType(StrToInt(TokenType)));
      end;

      if Command = 'SetUserPIN' then
      begin
        SmartCardID := GetJSONStringParameter(jsonInput, 'SmartCardID');
        AdministratorPIN := GetJSONStringParameter(jsonInput, 'AdministratorPIN');
        UserPIN := GetJSONStringParameter(jsonInput, 'NewUserPIN');
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        SetUserPIN(SmartCardID, AdministratorPIN, UserPIN, TTokenType(StrToInt(TokenType)));
      end;

      if Command = 'FormatToken' then
      begin
        SmartCardID := GetJSONStringParameter(jsonInput, 'SmartCardID');
        TokenName := GetJSONStringParameter(jsonInput, 'TokenName');
        CurrentAdministratorPIN := GetJSONStringParameter(jsonInput, 'CurrentAdministratorPIN');
        NewAdministratorPIN := GetJSONStringParameter(jsonInput, 'NewAdministratorPIN');
        NewUserPIN := GetJSONStringParameter(jsonInput, 'NewUserPIN');
        CurInitKey := GetJSONStringParameter(jsonInput, 'CurInitKey');
        NewInitKey := GetJSONStringParameter(jsonInput, 'NewInitKey');
        if not TryStrToInt(GetJSONStringParameter(jsonInput, 'AdministratorPINTriesCount'), AdministratorPINTriesCount) then
          AdministratorPINTriesCount := 15;
        if not TryStrToInt(GetJSONStringParameter(jsonInput, 'UserPINTriesCount'), UserPINTriesCount) then
          UserPINTriesCount := 15;
        TokenType := GetJSONStringParameter(jsonInput, 'TokenType');

        FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
          NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey, AdministratorPINTriesCount,
          UserPINTriesCount, TTokenType(StrToInt(TokenType)));
      end;

      // ErrorCode = 0 - all OK
      jsonResult.AddPair(TJSONPair.Create('ErrorCode', TJSONNumber.Create(0)));
    finally
      jsonInput.Free;
    end;
  except
    on E: ETokenAccessException do
    begin
      jsonResult.AddPair(TJSONPair.Create('ErrorCode', TJSONNumber.Create(E.ErrorCode)));
      jsonResult.AddPair(TJSONPair.Create('ErrorMessage', TTokenAccess.GetErrorMessageByCode(E.ErrorCode)));

      // Write to logfile
      LogMsg := ' ErrorCode: ' + IntToStr(E.ErrorCode);
      if E.NativeErrorCode <> 0 then
        LogMsg := LogMsg + '; NativeErrorCode: ' + IntToStr(E.NativeErrorCode);
      LogMsg := LogMsg + '; Error message: ' + TTokenAccess.GetErrorMessageByCode(E.ErrorCode);
      TLogger.WriteLog(LogMsg);
    end;
  end;

  Result := jsonResult.ToString;
  jsonResult.Free;
end;

procedure TTokenAccess.SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN,
  NewAdministratorPIN: String; TokenType: TTokenType);
var Token: TToken;
begin
  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    Token.SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN,
      NewAdministratorPIN);
  finally
    Token.Free;
  end;
end;

procedure TTokenAccess.SetTokenName(SmartCardID, TokenName, UserPIN: String;
  TokenType: TTokenType);
var Token: TToken;
begin
  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    Token.SetTokenName(SmartCardID, TokenName, UserPIN);
  finally
    Token.Free;
  end;
end;

procedure TTokenAccess.SetUserPIN(SmartCardID, AdministratorPIN,
  NewUserPIN: String; TokenType: TTokenType);
var Token: TToken;
begin
  case TokenType of
    ruToken: Token := TRuToken.Create;
    iKey: Token := TIKey.Create;
    eToken: Token := TEToken.Create;
    else raise ETokenAccessException.Create(28);
  end;
  try
    Token.SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN);
  finally
    Token.Free;
  end;
end;

{ ETokenAccessException }

constructor ETokenAccessException.Create(ErrorCode, NativeErrorCode: Word);
begin
  inherited Create('');

  FErrorCode := ErrorCode;
  FNativeErrorCode := NativeErrorCode;
end;

{ TToken }

constructor TToken.Create;
begin
  inherited;

  TokensList := TList<RToken>.Create;
end;

destructor TToken.Destroy;
begin
  TokensList.Free;

  UnloadPKCS11Dll;
end;

function TToken.GetStringFromByteArray(ByteArray: array of byte): String;
var i: Integer;
begin
  Result := '';

  for i := 0 to Length(ByteArray) - 1 do
    Result := Result + Chr(ByteArray[i]);

  Result := trim(Result);
end;

function TToken.GetStringFromUTF8ByteArray(ByteArray: array of Byte): String;
var Bytes: TBytes;
begin
  SetLength(Bytes, 32);
  CopyMemory(@Bytes[0], @ByteArray[0], 32);

  Result := trim(TEncoding.UTF8.GetString(Bytes));

  SetLength(Bytes, 0);
end;

function TToken.GetTokenBySmartCardID(SmartCardID: String): RToken;
var i: Integer;
    TokenFound: Boolean;
begin
  ListTokens;

  TokenFound := False;

  for i := 0 to TokensList.Count - 1 do
  if TokensList[i].SmartCardID = SmartCardID then
  begin
    Result := TokensList[i];

    TokenFound := True;
    break;
  end;

  if not TokenFound then
    raise ETokenAccessException.Create(5);
end;

procedure TToken.ListTokens;
var slotCount: CK_ULONG;
    i: Integer;
    slotInfo: CK_SLOT_INFO;
    tokenInfo: CK_TOKEN_INFO;
    Token: RToken;
    ErrRet: CK_RV;
    slotList: array of CK_ULONG;
begin
  // Initialize PKCS#11 library
  TLogger.WriteLog('Initializing PKCS#11 library.');
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);
  TLogger.WriteLog('Success.');

  try
    // Get slots count
    TLogger.WriteLog('Getting slots count.');
    ErrRet := C_GetSlotList(CK_FALSE, nil, @slotCount);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(2, ErrRet);
    TLogger.WriteLog('Success. Slots count: ' + IntToStr(slotCount));

    // GEt memory fro slots list
    SetLength(slotList, slotCount);

    // Get slots
    TLogger.WriteLog('Getting slots list.');
    ErrRet := C_GetSlotList(CK_FALSE, @slotList[0], @slotCount);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(2, ErrRet);
    TLogger.WriteLog('Success.');

    // Enumerate slots
    for i := 0 to slotCount - 1 do
    begin
      // Get slot info
      TLogger.WriteLog('Getting slot info. Slot #' + IntToStr(i));
      ErrRet := C_GetSlotInfo(slotList[i], @slotInfo);
      if ErrRet <> CKR_OK then
        raise ETokenAccessException.Create(3, ErrRet);
      TLogger.WriteLog('Success.');

      // If token present in slot
      if (slotInfo.flags and CKF_TOKEN_PRESENT) <> 0 then
      begin
        // Get token information
        TLogger.WriteLog('Getting token info.');
        ErrRet := C_GetTokenInfo(slotList[i], @tokenInfo);
        if (ErrRet <> CKR_OK) then
        begin
          if TokenType <> iKey then
            raise ETokenAccessException.Create(4, ErrRet);

          // Because C_GetTokenInfo returns CKR_TOKEN_NOT_RECOGNIZED also when PKI storage wasn't initialized
          // See chapter 6 - Calling Uninitialized iKeys in iKey 1000 SDK
          if (TokenType = iKey) and (ErrRet <> CKR_TOKEN_NOT_RECOGNIZED) then
            raise ETokenAccessException.Create(4, ErrRet);
        end;
        TLogger.WriteLog('Success.');

        Token.SmartCardID := GetStringFromByteArray(tokenInfo.serialNumber);
        Token.slotID := slotList[i];
        Token.TokenModel := GetStringFromByteArray(tokenInfo.model);
        Token.TokenType := TokenType;
        Token.TokenName := GetStringFromUTF8ByteArray(tokenInfo._label);

        // Add it to list
        TokensList.Add(Token);
      end;
    end;
  finally
    TLogger.WriteLog('Finalizing PKCS#11 library.');
    C_Finalize(nil);
    TLogger.WriteLog('Success.');
  end;
end;

procedure TToken.ListTokensToJSON(jsonResult: TJSONObject);
var jsonToken: TJSONObject;
    jsonTokensArray: TJSONArray;
    i: Integer;
begin
  ListTokens;

  // Create JSON object from TokensList
  jsonTokensArray := TJSONArray.Create;
  for i := 0 to TokensList.Count - 1 do
  begin
    jsonToken := TJSONObject.Create;
    jsonToken.AddPair('SmartCardID', TokensList[i].SmartCardID);
    jsonToken.AddPair('TokenType', TJSONNumber.Create(Byte(TokensList[i].TokenType)));
    jsonToken.AddPair('TokenModel', TokensList[i].TokenModel);

    // Add this JSON object to the array object
    jsonTokensArray.AddElement(jsonToken);
  end;
  // Add array to result
  jsonResult.AddPair(TJSONPair.Create('Tokens', jsonTokensArray));
end;

procedure TToken.LoadPKCS11Dll;
begin
  PKCS11LibHandle := LoadLibrary(PWideChar(PKCS11LibName));
  if PKCS11LibHandle < 32 then
    raise ETokenAccessException.Create(12);

  @C_Initialize := GetProcAddress(PKCS11LibHandle, 'C_Initialize');
  if @C_Initialize = nil then
    raise ETokenAccessException.Create(13);

  @C_GetSlotList := GetProcAddress(PKCS11LibHandle, 'C_GetSlotList');
  if @C_GetSlotList = nil then
    raise ETokenAccessException.Create(13);

  @C_GetSlotInfo := GetProcAddress(PKCS11LibHandle, 'C_GetSlotInfo');
  if @C_GetSlotInfo = nil then
    raise ETokenAccessException.Create(13);

  @C_GetTokenInfo := GetProcAddress(PKCS11LibHandle, 'C_GetTokenInfo');
  if @C_GetTokenInfo = nil then
    raise ETokenAccessException.Create(13);

  @C_Finalize := GetProcAddress(PKCS11LibHandle, 'C_Finalize');
  if @C_Finalize = nil then
    raise ETokenAccessException.Create(13);

  @C_OpenSession := GetProcAddress(PKCS11LibHandle, 'C_OpenSession');
  if @C_OpenSession = nil then
    raise ETokenAccessException.Create(13);

  @C_CloseSession := GetProcAddress(PKCS11LibHandle, 'C_CloseSession');
  if @C_CloseSession = nil then
    raise ETokenAccessException.Create(13);

  @C_Login := GetProcAddress(PKCS11LibHandle, 'C_Login');
  if @C_Login = nil then
    raise ETokenAccessException.Create(13);

  @C_Logout := GetProcAddress(PKCS11LibHandle, 'C_Logout');
  if @C_Logout = nil then
    raise ETokenAccessException.Create(13);

  @C_InitPIN := GetProcAddress(PKCS11LibHandle, 'C_InitPIN');
  if @C_InitPIN = nil then
    raise ETokenAccessException.Create(13);

  @C_SetPIN := GetProcAddress(PKCS11LibHandle, 'C_SetPIN');
  if @C_SetPIN = nil then
    raise ETokenAccessException.Create(13);

  @C_InitToken := GetProcAddress(PKCS11LibHandle, 'C_InitToken');
  if @C_InitToken = nil then
    raise ETokenAccessException.Create(13);

  @C_FindObjectsInit := GetProcAddress(PKCS11LibHandle, 'C_FindObjectsInit');
  if @C_FindObjectsInit = nil then
    raise ETokenAccessException.Create(13);

  @C_FindObjects := GetProcAddress(PKCS11LibHandle, 'C_FindObjects');
  if @C_FindObjects = nil then
    raise ETokenAccessException.Create(13);

  @C_FindObjectsFinal := GetProcAddress(PKCS11LibHandle, 'C_FindObjectsFinal');
  if @C_FindObjectsFinal = nil then
    raise ETokenAccessException.Create(13);

  @C_CreateObject := GetProcAddress(PKCS11LibHandle, 'C_CreateObject');
  if @C_CreateObject = nil then
    raise ETokenAccessException.Create(13);

  @C_DestroyObject := GetProcAddress(PKCS11LibHandle, 'C_DestroyObject');
  if @C_DestroyObject = nil then
    raise ETokenAccessException.Create(13);

  @C_DigestInit := GetProcAddress(PKCS11LibHandle, 'C_DigestInit');
  if @C_DigestInit = nil then
    raise ETokenAccessException.Create(13);

  @C_Digest := GetProcAddress(PKCS11LibHandle, 'C_Digest');
  if @C_Digest = nil then
    raise ETokenAccessException.Create(13);
end;

procedure TToken.SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN,
  NewAdministratorPIN: String);
var Token: RToken;
    hSession: CK_SESSION_HANDLE;
    CurrentAdministratorPINArray: TBytes;
    NewAdministratorPINArray: TBytes;
    ErrRet: CK_RV;
begin
  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    // Open session with token
    ErrRet := C_OpenSession(Token.slotID, CKF_RW_SESSION or CKF_SERIAL_SESSION, nil, nil, @hSession);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(6, ErrRet);

    CurrentAdministratorPINArray := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);
    NewAdministratorPINArray := TEncoding.UTF8.GetBytes(NewAdministratorPIN);

    // Login to token
    ErrRet :=  C_Login(hSession, CKU_SO, @CurrentAdministratorPINArray[0], Length(CurrentAdministratorPINArray));
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(7, ErrRet);

    // Set Administrator PIN
    ErrRet := C_SetPIN(hSession, @CurrentAdministratorPINArray[0], Length(CurrentAdministratorPINArray),
      @NewAdministratorPINArray[0], Length(NewAdministratorPINArray));
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(8, ErrRet);

    // Logout from token
    ErrRet := C_Logout(hSession);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(9, ErrRet);

    // Close session with token
    ErrRet := C_CloseSession(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(10, ErrRet);
  finally
    C_Finalize(nil);
  end;
end;

procedure TToken.SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN: String);
var Token: RToken;
    hSession: CK_SESSION_HANDLE;
    UTF8ByteArray: TBytes;
    ErrRet: CK_RV;
begin
  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    // Open session with token
    ErrRet := C_OpenSession(Token.slotID, CKF_RW_SESSION or CKF_SERIAL_SESSION, nil, nil, @hSession);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(6, ErrRet);

    UTF8ByteArray := TEncoding.UTF8.GetBytes(AdministratorPIN);
    // Login to token
    ErrRet := C_Login(hSession, CKU_SO, @UTF8ByteArray[0], Length(UTF8ByteArray));
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(7);

    // Set User PIN
    UTF8ByteArray := TEncoding.UTF8.GetBytes(NewUserPIN);
    ErrRet := C_InitPIN(hSession, @UTF8ByteArray[0], Length(UTF8ByteArray));
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(8, ErrRet);

    // Logout from token
    ErrRet := C_Logout(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(9, ErrRet);

    // Close session with token
    ErrRet := C_CloseSession(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(10, ErrRet);
  finally
    C_Finalize(nil);
  end;
end;

procedure TToken.UnloadPKCS11Dll;
begin
  FreeLibrary(PKCS11LibHandle);
end;

{ TRuToken }

constructor TRuToken.Create;
begin
  TokenType := ruToken;
  PKCS11LibName := ruTokenLib;

  inherited;

  LoadPKCS11Dll;
end;

procedure TRuToken.FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
  NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String; AdministratorPINTriesCount,
  UserPINTriesCount: Integer);
var InitInfo: CK_RUTOKEN_INIT_PARAM;
    TokenNameArray: TBytes;
    NewUserPINArray: TBytes;
    NewAdministratorPINArray: TBytes;
    CurrentAdministratorPINArray: TBytes;
    ErrRet: CK_RV;
    Token: RToken;
begin
  if (AdministratorPINTriesCount > 10) or (UserPINTriesCount > 10) then
    raise ETokenAccessException.Create(24);

  if (Length(NewAdministratorPIN) < 6) or (Length(NewAdministratorPIN) > 32) or
     (Length(NewUserPIN) < 6) or (Length(NewUserPIN) > 32) then
    raise ETokenAccessException.Create(25);

  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    // Fill initialization structure
    FillChar(InitInfo, SizeOf(InitInfo), 0);

    InitInfo.ulSizeofThisStructure := SizeOf(InitInfo);
    InitInfo.UseRepairMode := 0;
    InitInfo.ChangeUserPINPolicy := 3;

    if Token.TokenModel = 'Rutoken S' then
    begin
      InitInfo.ulMinAdminPinLen := 1;
      InitInfo.ulMinUserPinLen := 1;
    end else
    begin
      InitInfo.ulMinAdminPinLen := 6;
      InitInfo.ulMinUserPinLen := 6;
    end;

    // Token Name
    if TokenName = '' then
      TokenName := 'URALSIB';
    TokenNameArray := TEncoding.UTF8.GetBytes(TokenName);
    InitInfo.pTokenLabel := @TokenNameArray[0];
    InitInfo.ulLabelLen := Length(TokenNameArray);

    // User PIN
    if NewUserPIN = '' then
      NewUserPIN := '12345678';
    NewUserPINArray := TEncoding.UTF8.GetBytes(NewUserPIN);
    InitInfo.pNewUserPin := @NewUserPINArray[0];
    InitInfo.ulNewUserPinLen := Length(NewUserPINArray);

    // Administrator PIN
    if NewAdministratorPIN = '' then
      NewAdministratorPIN := '87654321';
    NewAdministratorPINArray := TEncoding.UTF8.GetBytes(NewAdministratorPIN);
    InitInfo.pNewAdminPin := @NewAdministratorPINArray[0];
    InitInfo.ulNewAdminPinLen := Length(NewAdministratorPINArray);

    // User PIN tries count
    InitInfo.ulMaxUserRetryCount := UserPINTriesCount;

    // Administrator PIN tries count
    InitInfo.ulMaxAdminRetryCount := AdministratorPINTriesCount;

    // Current Administrator PIN
    CurrentAdministratorPINArray := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);

    ErrRet := RuToken_C_EX_InitToken(Token.slotID, @CurrentAdministratorPINArray[0],
      Length(CurrentAdministratorPINArray), @InitInfo);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(14, ErrRet);
  finally
    C_Finalize(nil);
  end;
end;

procedure TRuToken.LoadPKCS11Dll;
begin
  inherited;

  @RuToken_C_EX_SetTokenName := GetProcAddress(PKCS11LibHandle, 'C_EX_SetTokenName');
  if @RuToken_C_EX_SetTokenName = nil then
    raise ETokenAccessException.Create(13);

  @RuToken_C_EX_InitToken := GetProcAddress(PKCS11LibHandle, 'C_EX_InitToken');
  if @RuToken_C_EX_InitToken = nil then
    raise ETokenAccessException.Create(13);
end;

procedure TRuToken.SetTokenName(SmartCardID, TokenName, UserPIN: String);
var Token: RToken;
    hSession: CK_SESSION_HANDLE;
    UTF8ByteArray: TBytes;
    ErrRet: CK_RV;
begin
  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    // Open session with token
    ErrRet := C_OpenSession(Token.slotID, CKF_RW_SESSION or CKF_SERIAL_SESSION, nil, nil, @hSession);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(6, ErrRet);

    UTF8ByteArray := TEncoding.UTF8.GetBytes(UserPIN);
    // Login to token
    ErrRet := C_Login(hSession, CKU_USER, @UTF8ByteArray[0], Length(UTF8ByteArray));
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(7, ErrRet);

    // Set token name
    UTF8ByteArray := TEncoding.UTF8.GetBytes(TokenName);
    ErrRet := RuToken_C_EX_SetTokenName(hSession, @UTF8ByteArray[0], Length(UTF8ByteArray));
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(8, ErrRet);

    // Logout from token
    ErrRet := C_Logout(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(9, ErrRet);

    // Close session with token
    ErrRet := C_CloseSession(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(10, ErrRet);
  finally
    C_Finalize(nil);
  end;
end;

{ TIKey }

constructor TIKey.Create;
begin
  TokenType := iKey;
  PKCS11LibName := iKeyTokenLib;

  inherited;

  LoadPKCS11Dll;
  LoadIKeyAPIDll;
end;

destructor TIKey.Destroy;
begin
  UnloadIKeyAPIDll;

  inherited;
end;

procedure TIKey.FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
  NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
  AdministratorPINTriesCount, UserPINTriesCount: Integer);
var iHandle: THandle;
    UTF8ByteArray: TBytes;
    ErrRet: Integer;
    iKeySerial: array [0..1] of CK_ULONG;
    Token: RToken;
begin
  TLogger.WriteLog('Start formatting iKey.');

  if (Length(NewAdministratorPIN) < 6) or (Length(NewAdministratorPIN) > 25) then
    raise ETokenAccessException.Create(31);

  if (Length(NewUserPIN) < 1) or (Length(NewUserPIN) > 10) or (not TryStrToInt(NewUserPIN, ErrRet)) then
    raise ETokenAccessException.Create(32);

  TLogger.WriteLog('Finding token.');
  Token := GetTokenBySmartCardID(SmartCardID);
  TLogger.WriteLog('Token was found.');

  TLogger.WriteLog('Creating iKey context.');
  if iKey_CreateContext(@iHandle, 0, iKeyAPIVersion) <> 0 then
    raise ETokenAccessException.Create(18);
  TLogger.WriteLog('Success.');

  TLogger.WriteLog('Opening iKey device.');
  iKeySerial[1] := StrToInt('$' + Copy(SmartCardID, 1, 8));
  iKeySerial[0] := StrToInt('$' + Copy(SmartCardID, 9, 8));
  ErrRet := iKey_OpenDevice(iHandle, IKEY_OPEN_SPECIFIC, @iKeySerial[0]);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(19);
  TLogger.WriteLog('Success.');

  try
    UTF8ByteArray := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);
    // Verify administrator PIN
    TLogger.WriteLog('Verifying administrator PIN.');
    if iKey_Verify(iHandle, IKEY_VERIFY_SO_PIN, @UTF8ByteArray[0], Length(UTF8ByteArray)) <> 0 then
      raise ETokenAccessException.Create(26);
    TLogger.WriteLog('Success.');

    // Remove PKI Storage
    TLogger.WriteLog('Erasing token memory.');
    if iKey_DeleteDir(iHandle, IKEY_DIR_BY_LONG_ID, IKEY_ROOT_DIR, nil) <> 0  then
      raise ETokenAccessException.Create(27);
    TLogger.WriteLog('Success.');
  finally
    TLogger.WriteLog('Closing iKey device.');
    if iKey_CloseDevice(iHandle) <> 0 then
      raise ETokenAccessException.Create(21);
    TLogger.WriteLog('Success.');
  end;

  { For now URALSIB isn't use PKCS#11 storage
  try
    // Initialize PKCS#11 library
    TLogger.WriteLog('Initializing PKCS#11 library.');
    ErrRet := C_Initialize(nil);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(1, ErrRet);
    TLogger.WriteLog('Success.');

    // Init token
    UTF8ByteArray := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);
    TokenNameByteArray := TEncoding.UTF8.GetBytes(TokenName);
    SetLength(TokenNameByteArray, 32);
    TLogger.WriteLog('Initializing PKCS#11 storage.');
    ErrRet := C_InitToken(Token.slotID, @UTF8ByteArray[0], Length(UTF8ByteArray), @TokenNameByteArray[0]);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(30, ErrRet);
    TLogger.WriteLog('Success.');
  finally
    TLogger.WriteLog('Uninitializing PKCS#11 library.');
    C_Finalize(nil);
    TLogger.WriteLog('Success.');
  end;
  }

  // Set administrator PIN
  TLogger.WriteLog('Setting administrator PIN.');
  SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN, NewAdministratorPIN);
  TLogger.WriteLog('Success.');

  // Set user PIN
  TLogger.WriteLog('Setting user PIN.');
  SetUserPIN(SmartCardID, NewAdministratorPIN, NewUserPIN);
  TLogger.WriteLog('Success.');

  // Set token name
  TLogger.WriteLog('Setting token name.');
  SetTokenName(SmartCardID, TokenName, NewUserPIN);
  TLogger.WriteLog('Success.');

  TLogger.WriteLog('iKey was successfully formatted.');
end;

procedure TIKey.LoadIKeyAPIDll;
begin
  iKeyAPILibHandle := LoadLibrary(PWideChar(iKeyAPILib));
  if iKeyAPILibHandle < 32 then
    raise ETokenAccessException.Create(15);

  @iKey_CreateContext := GetProcAddress(iKeyAPILibHandle, 'ikey_CreateContext');
  if @iKey_CreateContext = nil then
    raise ETokenAccessException.Create(16);

  @iKey_OpenDevice := GetProcAddress(iKeyAPILibHandle, 'ikey_OpenDevice');
  if @iKey_OpenDevice = nil then
    raise ETokenAccessException.Create(16);

  @iKey_CloseDevice := GetProcAddress(iKeyAPILibHandle, 'ikey_CloseDevice');
  if @iKey_CloseDevice = nil then
    raise ETokenAccessException.Create(16);

  @iKey_SetProperty := GetProcAddress(iKeyAPILibHandle, 'ikey_SetProperty');
  if @iKey_SetProperty = nil then
    raise ETokenAccessException.Create(16);

  @iKey_GetProperty := GetProcAddress(iKeyAPILibHandle, 'ikey_GetProperty');
  if @iKey_GetProperty = nil then
    raise ETokenAccessException.Create(16);

  @iKey_Verify := GetProcAddress(iKeyAPILibHandle, 'ikey_Verify');
  if @iKey_Verify = nil then
    raise ETokenAccessException.Create(16);

  @iKey_DeleteDir := GetProcAddress(iKeyAPILibHandle, 'ikey_DeleteDir');
  if @iKey_DeleteDir = nil then
    raise ETokenAccessException.Create(16);

  @iKey_ChangeCode := GetProcAddress(iKeyAPILibHandle, 'ikey_ChangeCode');
  if @iKey_ChangeCode = nil then
    raise ETokenAccessException.Create(16);
end;

procedure TIKey.SetAdministratorPIN(SmartCardID, CurrentAdministratorPIN,
  NewAdministratorPIN: String);
var iHandle: THandle;
    CurrentPINUTF8, NewPINUTF8: TBytes;
    ErrRet: Integer;
    iKeySerial: array [0..1] of CK_ULONG;
begin
  ErrRet := iKey_CreateContext(@iHandle, 0, iKeyAPIVersion);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(18, ErrRet);

  iKeySerial[1] := StrToInt('$' + Copy(SmartCardID, 1, 8));
  iKeySerial[0] := StrToInt('$' + Copy(SmartCardID, 9, 8));
  ErrRet := iKey_OpenDevice(iHandle, IKEY_OPEN_SPECIFIC, @iKeySerial[0]);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(19, ErrRet);

  CurrentPINUTF8 := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);
  NewPINUTF8 := TEncoding.UTF8.GetBytes(NewAdministratorPIN);
  ErrRet := ikey_ChangeCode(iHandle, IKEY_CHANGE_SO_PIN, @CurrentPINUTF8[0],
              Length(CurrentPINUTF8), @NewPINUTF8[0], Length(NewPINUTF8));
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(29, ErrRet);

  ErrRet := iKey_CloseDevice(iHandle);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(21, ErrRet);
end;

procedure TIKey.SetTokenName(SmartCardID, TokenName, UserPIN: String);
var iHandle: THandle;
    UTF8ByteArray: TBytes;
    ErrRet: Integer;
    iKeySerial: array [0..1] of CK_ULONG;
begin
  ErrRet := iKey_CreateContext(@iHandle, 0, iKeyAPIVersion);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(18, ErrRet);

  iKeySerial[1] := StrToInt('$' + Copy(SmartCardID, 1, 8));
  iKeySerial[0] := StrToInt('$' + Copy(SmartCardID, 9, 8));
  ErrRet := iKey_OpenDevice(iHandle, IKEY_OPEN_SPECIFIC, @iKeySerial[0]);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(19, ErrRet);

  UTF8ByteArray := TEncoding.UTF8.GetBytes(TokenName);
  ErrRet := iKey_SetProperty(iHandle, IKEY_PROP_FRIENDLY_NAME, nil, @UTF8ByteArray[0], Length(UTF8ByteArray));
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(20, ErrRet);

  ErrRet := iKey_CloseDevice(iHandle);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(21, ErrRet);
end;

procedure TIKey.SetUserPIN(SmartCardID, AdministratorPIN, NewUserPIN: String);
var iHandle: THandle;
    AdministratorPINUTF8: TBytes;
    ErrRet: Integer;
    iKeySerial: array [0..1] of CK_ULONG;
    NewUserPININT: Integer;
begin
  ErrRet := iKey_CreateContext(@iHandle, 0, iKeyAPIVersion);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(18, ErrRet);

  iKeySerial[1] := StrToInt('$' + Copy(SmartCardID, 1, 8));
  iKeySerial[0] := StrToInt('$' + Copy(SmartCardID, 9, 8));
  ErrRet := iKey_OpenDevice(iHandle, IKEY_OPEN_SPECIFIC, @iKeySerial[0]);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(19, ErrRet);

  AdministratorPINUTF8 := TEncoding.UTF8.GetBytes(AdministratorPIN);
  // See how it works in "User PIN Guidelines" in iKeyDevGuide.pdf
  NewUserPININT := StrToInt('$' + NewUserPIN);
  ErrRet := ikey_ChangeCode(iHandle, IKEY_UNBLOCK_USER_PIN, @AdministratorPINUTF8[0],
              Length(AdministratorPINUTF8), @NewUserPININT, Length(NewUserPIN));
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(29, ErrRet);

  ErrRet := iKey_CloseDevice(iHandle);
  if ErrRet <> 0 then
    raise ETokenAccessException.Create(21, ErrRet);
end;

procedure TIKey.UnloadIKeyAPIDll;
begin
  FreeLibrary(iKeyAPILibHandle);
end;

{ TEToken }

constructor TEToken.Create;
begin
  TokenType := eToken;
  PKCS11LibName := eTokenLib;

  inherited;

  LoadPKCS11Dll;
  LoadSAPIDll;
end;

procedure TEToken.CreateKeyObject(hSession: CK_SESSION_HANDLE; pLabel,
  pKey: String);
var tKey: array [0..3] of CK_ATTRIBUTE;
    cko_SecretKey, ckk_Des2: CK_ULONG;
    keyValue: array [0..15] of Byte;
    hObject: CK_OBJECT_HANDLE;
    ErrRet: CK_RV;
    pLabelArray: TBytes;
begin
  cko_SecretKey := CKO_SECRET_KEY;
  ckk_Des2 := CKK_DES2;

  tKey[0]._type := CKA_CLASS;
  tKey[0].pValue := @cko_SecretKey;
  tKey[0].ulValueLen := SizeOf(CK_ULONG);

  tKey[1]._type := CKA_KEY_TYPE;
  tKey[1].pValue := @ckk_Des2;
  tKey[1].ulValueLen := SizeOf(CK_ULONG);

  pLabelArray := TEncoding.UTF8.GetBytes(pLabel);
  tKey[2]._type := CKA_LABEL;
  tKey[2].pValue := @pLabelArray[0];
  tKey[2].ulValueLen := Length(pLabelArray);

  HashStartKey(pKey, @keyValue, Length(keyValue));

  tKey[3]._type := CKA_VALUE;
  tKey[3].pValue := @keyValue;
  tKey[3].ulValueLen := Length(keyValue);

  ErrRet := C_CreateObject(hSession, @tKey[0], Length(tKey), @hObject);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(45, ErrRet);
end;

destructor TEToken.Destroy;
begin
  UnloadSAPIDll;

  inherited;
end;

procedure TEToken.FormatToken(SmartCardID, TokenName, CurrentAdministratorPIN,
  NewAdministratorPIN, NewUserPIN, CurInitKey, NewInitKey: String;
  AdministratorPINTriesCount, UserPINTriesCount: Integer);
var ErrRet: CK_RV;
    Token: RToken;
    CurrentAdministratorPINArray: TBytes;
    NewAdministratorPINArray: TBytes;
    NewUserPINArray: TBytes;
    TokenNameArray: TBytes;
    CurInitKeyArray, NewInitKeyArray: TBytes;
    tokenAttributes: array [0..6] of CK_ATTRIBUTE;
begin
  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    CurrentAdministratorPINArray := TEncoding.UTF8.GetBytes(CurrentAdministratorPIN);
    NewAdministratorPINArray := TEncoding.UTF8.GetBytes(NewAdministratorPIN);
    NewUserPINArray := TEncoding.UTF8.GetBytes(NewUserPIN);
    TokenNameArray := TEncoding.UTF8.GetBytes(TokenName);
    CurInitKeyArray := TEncoding.UTF8.GetBytes(CurInitKey);
    NewInitKeyArray := TEncoding.UTF8.GetBytes(NewInitKey);

    tokenAttributes[0]._type := CKA_SAPI_OLD_KEY;
    tokenAttributes[0].pValue := @CurInitKeyArray[0];
    tokenAttributes[0].ulValueLen := Length(CurInitKeyArray);

    tokenAttributes[1]._type := CKA_SAPI_NEW_KEY;
    tokenAttributes[1].pValue := @NewInitKeyArray[0];
    tokenAttributes[1].ulValueLen := Length(NewInitKeyArray);

    tokenAttributes[2]._type := CKA_SAPI_PIN_CURRENT;
    tokenAttributes[2].pValue := @CurrentAdministratorPINArray[0];
    tokenAttributes[2].ulValueLen := Length(CurrentAdministratorPINArray);

    tokenAttributes[3]._type := CKA_SAPI_PIN_SO;
    tokenAttributes[3].pValue := @NewAdministratorPINArray[0];
    tokenAttributes[3].ulValueLen := Length(NewAdministratorPINArray);

    tokenAttributes[4]._type := CKA_SAPI_PIN_USER;
    tokenAttributes[4].pValue := @NewUserPINArray[0];
    tokenAttributes[4].ulValueLen := Length(NewUserPINArray);

    tokenAttributes[5]._type := CKA_SAPI_RETRY_USER_MAX;
    tokenAttributes[5].pValue := @UserPINTriesCount;
    tokenAttributes[5].ulValueLen := SizeOf(UserPINTriesCount);

    tokenAttributes[6]._type := CKA_SAPI_RETRY_SO_MAX;
    tokenAttributes[6].pValue := @AdministratorPINTriesCount;
    tokenAttributes[6].ulValueLen := SizeOf(AdministratorPINTriesCount);

    ErrRet := SAPI_InitToken(Token.slotID, @tokenAttributes[0], Length(tokenAttributes), nil, nil);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(33, ErrRet);

    // Set token name
    SetTokenName(SmartCardID, TokenName, NewUserPIN);
  finally
    C_Finalize(nil);
  end;
end;

procedure TEToken.HashStartKey(buffer: String; pDigest: CK_BYTE_PTR;
  ulKeyLen: CK_ULONG);
var hSession: CK_SESSION_HANDLE;
    digest_mechanism: CK_MECHANISM;
    ErrRet: CK_RV;
    bufferArray: TBytes;
begin
  digest_mechanism.mechanism := CKM_MD5;
  digest_mechanism.pParameter := nil;
  digest_mechanism.ulParameterLen := 0;

  ErrRet := eTokenFunctionsList.ETC_CreateVirtualSession(@hSession);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(42, ErrRet);

  ErrRet := C_DigestInit(hSession, @digest_mechanism);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(43, ErrRet);

  bufferArray := TEncoding.UTF8.GetBytes(buffer);
  ErrRet := C_Digest(hSession, @bufferArray[0], Length(bufferArray), pDigest, @ulKeyLen);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(44, ErrRet);

  ErrRet := C_CloseSession(hSession);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(10, ErrRet);
end;

procedure TEToken.LoadPKCS11Dll;
var ErrRet: Integer;
begin
  inherited;

  @ETC_GetFunctionListEx := GetProcAddress(PKCS11LibHandle, 'ETC_GetFunctionListEx');
  if @ETC_GetFunctionListEx = nil then
    raise ETokenAccessException.Create(13);

  ErrRet := ETC_GetFunctionListEx(@eTokenFunctionsList);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(38);
end;

procedure TEToken.LoadSAPIDll;
begin
  eTokenSAPILibHandle := LoadLibrary(PWideChar(SAPILib));
  if eTokenSAPILibHandle < 32 then
    raise ETokenAccessException.Create(46);

  @SAPI_SetTokenName := GetProcAddress(eTokenSAPILibHandle, 'SAPI_SetTokenName');
  if @SAPI_SetTokenName = nil then
    raise ETokenAccessException.Create(47);

  @SAPI_InitToken := GetProcAddress(eTokenSAPILibHandle, 'SAPI_InitToken');
  if @SAPI_InitToken = nil then
    raise ETokenAccessException.Create(47);
end;

procedure TEToken.SetTokenName(SmartCardID, TokenName, UserPIN: String);
var Token: RToken;
    hSession: CK_SESSION_HANDLE;
    UTF8ByteArray: TBytes;
    ErrRet: CK_RV;
begin
  Token := GetTokenBySmartCardID(SmartCardID);

  // Initialize PKCS#11 library
  ErrRet := C_Initialize(nil);
  if ErrRet <> CKR_OK then
    raise ETokenAccessException.Create(1, ErrRet);

  try
    // Open session with token
    ErrRet := C_OpenSession(Token.slotID, CKF_RW_SESSION or CKF_SERIAL_SESSION, nil, nil, @hSession);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(6, ErrRet);

    UTF8ByteArray := TEncoding.UTF8.GetBytes(UserPIN);
    // Login to token
    ErrRet := C_Login(hSession, CKU_USER, @UTF8ByteArray[0], Length(UTF8ByteArray));
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(7, ErrRet);

    // Set token name
    UTF8ByteArray := TEncoding.UTF8.GetBytes(TokenName + #0);
    ErrRet := SAPI_SetTokenName(hSession, @UTF8ByteArray[0]);
    if ErrRet <> CKR_OK then
      raise ETokenAccessException.Create(8, ErrRet);

    // Logout from token
    ErrRet := C_Logout(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(9, ErrRet);

    // Close session with token
    ErrRet := C_CloseSession(hSession);
    if ErrRet <> CKR_OK  then
      raise ETokenAccessException.Create(10, ErrRet);
  finally
    C_Finalize(nil);
  end;
end;

procedure TEToken.UnloadSAPIDll;
begin
  FreeLibrary(eTokenSAPILibHandle);
end;

{ TLogger }

class procedure TLogger.WriteLog(LogMsg: String);
var LogFile: String;
begin
  LogFile := TPath.GetTempPath + 'RainbowTokenAccess.log';
  if TFile.Exists(LogFile, True) then
  begin
    LogMsg := DateTimeToStr(Now) + ' ' + LogMsg + #13#10;

    TFile.AppendAllText(LogFile, LogMsg);
  end;
end;

end.
