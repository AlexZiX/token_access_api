unit wsService;

interface

uses sgcWebSocket_Classes, sgcWebSocket_Classes_Indy, sgcWebSocket_Server,
     sgcWebSocket, System.JSON, Winapi.Windows, pkcs11t, System.SysUtils,
     TokenAccess;

type
  TWSService = class
  private
    wsServer: TsgcWebsocketServer;
    procedure OnWSMessage(Connection: TsgcWSConnection; const Text: String);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TWSService }

constructor TWSService.Create;
begin
  inherited;

  // Create and start WebSocket server
  wsServer := TsgcWebsocketServer.Create(nil);

  wsServer.Port := 5000;
  wsServer.OnMessage := OnWSMessage;
  wsServer.Active := True;
end;

destructor TWSService.Destroy;
begin
  wsServer.Free;

  inherited;
end;

procedure TWSService.OnWSMessage(Connection: TsgcWSConnection;
  const Text: String);
var TokenAccess: TTokenAccess;
begin
  TokenAccess := TTokenAccess.Create;
  wsServer.Broadcast(TokenAccess.ProcessJSONRequest(Text));
  TokenAccess.Free;
end;

end.
