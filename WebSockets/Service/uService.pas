unit uService;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, System.Win.Registry, wsService;

type
  TTokenAccessService = class(TService)
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    wsService: TWSService;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  TokenAccessService: TTokenAccessService;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  TokenAccessService.Controller(CtrlCode);
end;

function TTokenAccessService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TTokenAccessService.ServiceAfterInstall(Sender: TService);
begin
  with TRegistry.Create(KEY_ALL_ACCESS) do
  try
    RootKey := HKEY_LOCAL_MACHINE;

    {Description under Windows 2000 and higher}
    if Win32MajorVersion >= 5 then
      if OpenKey('System\CurrentControlSet\Services\' + Name, False) then
      begin
        WriteString('Description', 'Rainbow Token Access Service for ruToken, iKey and eToken');
        WriteString('DependOnService', 'LANMANSERVER');
        CloseKey;
      end;

    //��� ������� Windows
    OpenKey('System\CurrentControlSet\Service\EventLog\Application\Rainbow Token Access Service', True);
    WriteInteger('CategoryCount', 2);
    WriteString('CategoryMessageFile', ParamStr(0));
    WriteString('EventMessageFile', ParamStr(0));
    WriteInteger('TypesSupported', EVENTLOG_SUCCESS or
                                   EVENTLOG_ERROR_TYPE or
                                   EVENTLOG_WARNING_TYPE or
                                   EVENTLOG_INFORMATION_TYPE);
  finally
    Free;
  end;
end;

procedure TTokenAccessService.ServiceAfterUninstall(Sender: TService);
begin
  with TRegistry.Create(KEY_ALL_ACCESS) do
  try
    RootKey := HKEY_LOCAL_MACHINE;

    {Description under Windows 2000 and higher}
    if Win32MajorVersion >= 5 then
      if OpenKey('System\CurrentControlSet\Services\' + Name, False) then
      begin
        DeleteValue('Description');
        CloseKey;
       end;
  finally
    Free;
  end;
end;

procedure TTokenAccessService.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  wsService := TWSService.Create;
end;

procedure TTokenAccessService.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  wsService.Free;
end;

end.
