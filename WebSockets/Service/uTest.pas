unit uTest;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, wsService, Vcl.StdCtrls,
  Vcl.Buttons;

type
  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    wsService: TWSService;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  try
    wsService := TWSService.Create;
  except
    // Because cracked version shows trial message sometimes at object creating
    wsService := TWSService.Create;
  end;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  wsService.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  BitBtn1Click(nil);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  BitBtn2Click(nil);
end;

end.
