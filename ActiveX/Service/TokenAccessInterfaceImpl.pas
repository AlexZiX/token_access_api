unit TokenAccessInterfaceImpl;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, RainbowTokenAccessInterface_TLB, StdVcl, TokenAccess,
  System.JSON;

type
  TTokenAccessInterface = class(TAutoObject, ITokenAccessInterface)
  private
    LastOperationResult: String;
  protected
    function Get_lastOperationResult: WideString; safecall;
    procedure processJSONRequest(const request: WideString); safecall;
  end;

implementation

uses ComServ;

function TTokenAccessInterface.Get_lastOperationResult: WideString;
begin
  Result := TBStr(LastOperationResult);
end;

procedure TTokenAccessInterface.processJSONRequest(const request: WideString);
var TokenAccess: TTokenAccess;
begin
  TokenAccess := TTokenAccess.Create;
  LastOperationResult := TokenAccess.ProcessJSONRequest(request);
  TokenAccess.Free;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TTokenAccessInterface, Class_TokenAccessInterface,
    ciMultiInstance, tmApartment);
end.
