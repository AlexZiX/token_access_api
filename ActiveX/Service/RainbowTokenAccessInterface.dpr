library RainbowTokenAccessInterface;

uses
  ComServ,
  RainbowTokenAccessInterface_TLB in 'RainbowTokenAccessInterface_TLB.pas',
  TokenAccessInterfaceImpl in 'TokenAccessInterfaceImpl.pas' {TokenAccessInterface: CoClass},
  TokenAccess in '..\..\WebSockets\Service\src\TokenAccess.pas',
  pkcs11t in '..\..\WebSockets\Service\src\pkcs11t.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
