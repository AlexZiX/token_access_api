unit RainbowTokenAccessInterface_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 25.12.2018 9:45:07 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Projects\TokenAccessAPI\ActiveX\Service\RainbowTokenAccessInterface (1)
// LIBID: {13FC76A4-89DF-4480-A690-619FF215D521}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  RainbowTokenAccessInterfaceMajorVersion = 1;
  RainbowTokenAccessInterfaceMinorVersion = 0;

  LIBID_RainbowTokenAccessInterface: TGUID = '{13FC76A4-89DF-4480-A690-619FF215D521}';

  IID_ITokenAccessInterface: TGUID = '{B7DC298B-D453-4E94-962F-7FB51A1A50D3}';
  CLASS_TokenAccessInterface: TGUID = '{6FF23D0A-272B-49E1-898C-1E5632527A37}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ITokenAccessInterface = interface;
  ITokenAccessInterfaceDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  TokenAccessInterface = ITokenAccessInterface;


// *********************************************************************//
// Interface: ITokenAccessInterface
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B7DC298B-D453-4E94-962F-7FB51A1A50D3}
// *********************************************************************//
  ITokenAccessInterface = interface(IDispatch)
    ['{B7DC298B-D453-4E94-962F-7FB51A1A50D3}']
    function Get_lastOperationResult: WideString; safecall;
    procedure processJSONRequest(const request: WideString); safecall;
    property lastOperationResult: WideString read Get_lastOperationResult;
  end;

// *********************************************************************//
// DispIntf:  ITokenAccessInterfaceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B7DC298B-D453-4E94-962F-7FB51A1A50D3}
// *********************************************************************//
  ITokenAccessInterfaceDisp = dispinterface
    ['{B7DC298B-D453-4E94-962F-7FB51A1A50D3}']
    property lastOperationResult: WideString readonly dispid 204;
    procedure processJSONRequest(const request: WideString); dispid 201;
  end;

// *********************************************************************//
// The Class CoTokenAccessInterface provides a Create and CreateRemote method to
// create instances of the default interface ITokenAccessInterface exposed by
// the CoClass TokenAccessInterface. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoTokenAccessInterface = class
    class function Create: ITokenAccessInterface;
    class function CreateRemote(const MachineName: string): ITokenAccessInterface;
  end;

implementation

uses System.Win.ComObj;

class function CoTokenAccessInterface.Create: ITokenAccessInterface;
begin
  Result := CreateComObject(CLASS_TokenAccessInterface) as ITokenAccessInterface;
end;

class function CoTokenAccessInterface.CreateRemote(const MachineName: string): ITokenAccessInterface;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TokenAccessInterface) as ITokenAccessInterface;
end;

end.

